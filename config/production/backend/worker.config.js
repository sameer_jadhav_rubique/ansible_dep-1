module.exports = {
	apps: [{
		name: 'WORKER',
		script: 'worker/Notification.js',
		watch: false,
		instances: 1,
		error_file: "/var/log/production/err.log",
		out_file: "/var/log/production/out.log",
		env: {
			COMMON_VARIABLE: 'true',
			PORT: 3001,
			NODE_ENV: "development"
		},
		env_production: {
			NODE_ENV: 'production',
			PORT: 3001
		},
		env_demo: {
			PORT: 3084,
			NODE_ENV: 'demo'
		}
	}]
}
