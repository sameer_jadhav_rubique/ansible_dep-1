module.exports = {
	apps: [{
		name: '{{app_name}}',
		script: 'bin/www.js',
		watch: false,
		exec_mode: "interpreter",
		error_file: "/var/log/{{app_name}}/err.log",
		out_file: "/var/log/{{app_name}}/out.log",
		"interpreter": 'babel-node',
		env: {
			COMMON_VARIABLE: 'true',
			PORT: '{{PORT}}',
			NODE_ENV: "development"
		}
	}]
}
